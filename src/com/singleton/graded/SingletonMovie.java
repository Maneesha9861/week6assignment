package com.singleton.graded;

import java.sql.*;

public class SingletonMovie
{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       Connection conn =SingletonPatternMovies.getSingletonMovies();
       try {
    	   String qurey="select * from Movies_Coming";
    	   Statement statement =conn.createStatement();
    	   ResultSet resultSet =statement.executeQuery(qurey);
    	   while(resultSet.next()) {
    		   System.out.println(resultSet.getInt(1)+" "+resultSet.getString(2)+" "+resultSet.getInt(3)+" "
    				   +resultSet.getNString(4));
    	   }
    	   
    	   
           System.out.println("-----------------------------------------------------\n");
    	   String qurey1="select * from Top_RatedIndia";
    	   Statement statement1 =conn.createStatement();
    	   ResultSet resultSet1 =statement1.executeQuery(qurey1);
    	   while(resultSet1.next()) {
    		   System.out.println(resultSet1.getInt(1)+"  "+resultSet1.getString(2)+"  "+
    	   resultSet1.getInt(3)+"  "+resultSet1.getNString(4));
    	   }
    	   
    	   System.out.println("-----------------------------------------------------\n");
    	   String qurey2="select * from Top_RatedMovies";
    	   Statement statement2 =conn.createStatement();
    	   ResultSet resultSet2 =statement2.executeQuery(qurey2);
    	   while(resultSet2.next()) {
    		   System.out.println(resultSet2.getInt(1)+"  "+resultSet2.getString(2)+"  "+
    	   resultSet2.getInt(3)+"  "+resultSet2.getNString(4));
    	   }
    	   
    	   System.out.println("-----------------------------------------------------\n");
    	   String qurey3="select * from Movies_inTheatres";
    	   Statement statement3 =conn.createStatement();
    	   ResultSet resultSet3 =statement3.executeQuery(qurey3);
    	   while(resultSet3.next()) {
    		   System.out.println(resultSet3.getInt(1)+"  "+resultSet3.getString(2)+"  "+
    	   resultSet3.getInt(3)+"  "+resultSet3.getNString(4));
    	   }
       }catch(Exception e) {
    	   System.out.println("got some error in connection");
       }
	}

}